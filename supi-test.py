#!/usr/bin/env python

# command:
# sudo nohup python supi.py &

import RPi.GPIO as gpio
import time

print("starting")
gpio.setmode(gpio.BCM)
gpio.setup(4, gpio.IN)

run=1
print("ready")
while run==1:

        if(gpio.input(4)):
            print('\033[92mH\033[0m')
        else:
            print('\033[91mL\033[0m')
        time.sleep(1)
