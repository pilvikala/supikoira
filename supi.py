#!/usr/bin/env python

# command:
# sudo nohup python supi.py &

import RPi.GPIO as gpio
import time
import datetime
import requests
import smtplib
import os
import dotenv

dotenv.load_dotenv()

smtpserver = os.environ['smtpserver']
print('smtp server: {}'.format(smtpserver))
login = os.environ['smtplogin']
print('login: {}'.format(login))
pwd = os.environ['smtppwd']
alertTo = os.environ['alertto']
print('alertTo: {}'.format(alertTo))
alertFrom = os.environ['alertfrom']
print('alertFrom: {}'.format(alertFrom))

def sendMessage(message):
    print('sending: ')
    print(message)
    msg = 'From: {}\r\nTo: {}\r\nSubject:{}\r\n\r\n{}'.format(alertFrom, alertTo, message, message)
    server = smtplib.SMTP_SSL(smtpserver)
    server.set_debuglevel(1)
    print('logging in')
    server.login(login, pwd)
    server.sendmail(alertFrom, alertTo, msg)
    server.quit()

#ignore alerts within this time frame from the last alert (in seconds):
alertDelta = 300

print("starting")
gpio.setmode(gpio.BCM)
gpio.setup(4, gpio.IN)

run=1
sendMessage('Supikoira has started')
print("ready")
lastAlertSent = time.time()
while run==1:

        if(gpio.input(4)):
            print("Detected motion")
            delta = time.time() - lastAlertSent
            print(delta)
            lastAlertSent = time.time()
            if(delta > alertDelta):
                sendMessage('Motion has been detected!')
            else:
                print('motion detected too soon, alert will not be sent')
        time.sleep(1)
